### Local commands ###

poetry:
	poetry lock --no-update && \
	poetry install

format:
	isort --skip-gitignore .
	black .

lint:
	isort --check --skip-gitignore .
	black --check .
	flake8 .

test:
	python -m pytest

# lint + test
check: lint test



### Docker commands for local deployment ###

up:
	docker compose -f ./infra/deploy_local/docker-compose_local.yaml up --build

# migrate + collectstatic
migrate:
	docker compose -f ./infra/deploy_local/docker-compose_local.yaml exec backend poetry run python manage.py migrate && \
	docker compose -f ./infra/deploy_local/docker-compose_local.yaml exec backend poetry run python manage.py collectstatic --no-input

createsuperuser:
	docker compose -f ./infra/deploy_local/docker-compose_local.yaml exec backend poetry run python manage.py createsuperuser

down:
	docker compose -f ./infra/deploy_local/docker-compose_local.yaml down