# Test Web Service Project Template

##### Run local server with docker-compose
You will need a **.env** file in **infra/deploy_local/** directory.
See the **/infra/deploy_local/.env.local.example** file (you can just rename it)

Сontainers will be up:
1. postgresql
2. nginx, which listen 80 port
3. backend, which rebuilding with actual code
```
make up
```

Then make migrations + collect static files:
```
make migrate
```

Then create superuser:
```
make createsuperuser
```

Accessible admin panel by address ```http://localhost/admin/```