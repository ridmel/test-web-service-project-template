FROM python:3.10.5-slim as base

ENV PYTHONUNBUFFERED=1 \
    PIP_DISABLE_PIP_VERSION_CHECK=1 \
    PIP_NO_CACHE_DIR=1 \
    POETRY_VIRTUALENVS_IN_PROJECT=true \
    POETRY_NO_INTERACTION=1 \
    POETRY_VERSION=1.5.1

WORKDIR /code
RUN python3 -m pip install --upgrade pip
RUN pip install "poetry==$POETRY_VERSION"
COPY manage.py setup.cfg ./
COPY config/ ./config/
COPY poetry.lock pyproject.toml ./


FROM base as prod_build
RUN poetry --no-dev install
COPY apps/ ./apps/
CMD poetry run gunicorn config.wsgi:application -c /code/config/gunicorn.conf.py


FROM base as dev_build
RUN poetry install
COPY tests/ ./tests/
COPY apps/ ./apps/
CMD poetry run gunicorn config.wsgi:application -c /code/config/gunicorn.conf.py